#!/bin/bash

podman run -d -p 8080:8080 -it --name sidekick --rm sidekick-fix --address :8080 --health-path=/minio/health/cluster https://node{1...8}.storage.dacs.utwente.nl
