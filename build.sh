#!/usr/bin/env bash

echo "BUILDING SIDEKICK IMAGE..."
podman build -f=Dockerfile.sidekick -t sidekick-fix

podman pull quay.io/minio/mc
# you will need to mount a volume
podman create --tty --net=host --name=mc_container quay.io/minio/mc  #-v $HOST_DIR:$CONTAINER_DIR
podman start mc_container
#podman exec mc_container mc alias set storage http://localhost:8080 ${MINIO_ACCESS_USER} ${MINIO_ACCESS_PASSWORD}

