# minio-container
## Name
Container solution for minio client and sidekick

## Description

## Installation
It is mandatory to export the following environment variables:  

```
MINIO_ACCESS_USER
MINIO_ACCESS_PASSWORD
```

Then build the image as:  

```
./build.sh
```

## Usage
The mc client can be used as follows:  

```
podman run --network=host --rm -v "$(pwd)":/root/ mc ls 
```


The sidekick can be started as follows:  

```
./start_sidekick
```

## License
MIT licence

